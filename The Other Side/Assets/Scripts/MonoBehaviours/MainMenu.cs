﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;
using TMPro;
public class MainMenu : MonoBehaviour
{
    public bool moveToSkins = false;
    public bool moveToMain = false;

    public Transform skinsMarker;
    public Transform mainMenuMarker;

    public GameObject mainMenuCam;
    public GameObject skinsCam;

    public TextMeshProUGUI highScore;

    public TextMeshProUGUI skinUnlockText;


    public GameManager gm;

    public float speed = 10;
    public int currentSkin = 0;
    public int[] skins =
    {
        1,
        0,
        0,
        0
    };

    public void Start(){
        Time.timeScale = 1f;
        highScore = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        highScore.text = gm.highScore.ToString();
        int numOfUnlockedSkins = 0;
        for(int i = 0; i < skins.Length; i++)
        {
            skins[i] = gm.unlockedSkins[i];
            if(skins[i] == 1)
            {
                numOfUnlockedSkins += 1;
            }
        }
        if(numOfUnlockedSkins == 1)
        {
            skinUnlockText.text = "Complete 10 levels in one game to unlock a new animal!";
        }
        else if(numOfUnlockedSkins == 2)
        {
            skinUnlockText.text = "Complete 25 levels in one game to unlock a new animal!";
        }
        else if(numOfUnlockedSkins == 3)
        {
            skinUnlockText.text = "Complete 50 levels in one game to unlock a new animal!";
        }
        else if (numOfUnlockedSkins == 4)
        {
            skinUnlockText.text = "You have unlocked all of the animals!";
        }
        currentSkin = gm.currentSkin;



    }

    private void Update()
    {
        if(moveToSkins)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, skinsMarker.position, 1 * Time.deltaTime * speed);
        }
        else if (moveToMain)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, mainMenuMarker.position, 1 * Time.deltaTime * speed);
        }
    }

    public void MoveToSkins()
    {
        moveToMain = false;
        moveToSkins = true;
    }

    public void MoveToMain()
    {
        moveToMain = true;
        moveToSkins = false;
    }

    public void PlayGame()
    {
        //Loads the next scene in sequence
        SceneManager.LoadScene(1);
    }

    //loads main menu
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }


    //quits the game
    public void QuitGame()
    {
        //Closes the application
        Application.Quit();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "SkinsMarker")
        {
            skinsCam.SetActive(true);
            this.transform.LookAt(mainMenuMarker);
        }
        else if(other.gameObject.name == "MainMarker")
        {
            this.transform.LookAt(skinsMarker);
            mainMenuCam.SetActive(true);
        }
    }

    public void GetNextSkin()
    {
        for(int i = currentSkin+1; i < skins.Length; i++)
        {
            if(skins[i] == 1)
            {
                this.transform.GetChild(currentSkin).gameObject.SetActive(false);
                this.transform.GetChild(i).gameObject.SetActive(true);
                currentSkin = i;
                gm.currentSkin = i;
                break;
            }
        }
    }

    public void GetPreviousSkin()
    {
        for (int i = currentSkin - 1; i >= 0; i--)
        {
            if (skins[i] == 1)
            {
                this.transform.GetChild(currentSkin).gameObject.SetActive(false);
                this.transform.GetChild(i).gameObject.SetActive(true);
                currentSkin = i;
                gm.currentSkin = i;
                break;
            }
        }
    }


}
