﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioButtons : MonoBehaviour
{
    public GameManager gm;
    // Start is called before the first frame update
    void Update()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        if(this.gameObject.name == "MusicOn" && !gm.musicMuted)
        {
            this.gameObject.SetActive(false);
        }
        else if(this.gameObject.name == "MusicOff" && gm.musicMuted)
        {
            this.gameObject.SetActive(false);
        }
    }
}
