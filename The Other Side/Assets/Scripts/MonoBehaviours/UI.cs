﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{
    //references the LevelGenerator script which is found on the levelGenerator GameObject in the scene
    private LevelGenerator lg;

    public TextMeshProUGUI levelNumber;

    // Start is called before the first frame update
    void Start()
    {
        levelNumber = this.GetComponent<TextMeshProUGUI>();
        lg = GameObject.FindGameObjectWithTag("LevelGenerator").GetComponent<LevelGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        levelNumber.text = lg.levelNumber.ToString();
    }

}
