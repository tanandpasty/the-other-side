﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EndMenu : MonoBehaviour
{
    public void PlayGame()
    {
        //Loads the next scene in sequence
        SceneManager.LoadScene(1);
    }

    //loads main menu
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }


    //quits the game
    public void QuitGame()
    {
        //Closes the application
        Application.Quit();
    }
}
