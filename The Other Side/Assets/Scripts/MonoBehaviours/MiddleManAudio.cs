﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleManAudio : MonoBehaviour
{
    public GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public void MusicOff()
    {
        gm.musicMuted = true;
    }

    public void MusicOn()
    {
        gm.musicMuted = false;
    }

}
