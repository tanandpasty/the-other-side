﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

public class spawner : MonoBehaviour
{
    // Any prefab object that you want to spawn (player or enemy in our game)
    public GameObject prefabToSpawn;

    // used to determine repeatInterval
    public float baseRepeat;

    // Used to spawn multiple copies at a set interval (primarily for enemy objects)
    public float repeatInterval;

    //Gets the level manager game object
    public LevelGenerator LM;

    public bool hasBeenEnabledBefore;

    public int levelNumber;
    private void Awake()
    {
        hasBeenEnabledBefore = false;
        LM = GameObject.FindGameObjectWithTag("LevelGenerator").GetComponent<LevelGenerator>();
    }

    private void Start()
    {
        if (LM != null && LM.levelNumber < 20)
        {
            repeatInterval = (baseRepeat - .075f * LM.GetComponent<LevelGenerator>().levelNumber);
        }
    }

    public void OnEnable()
    {
        if(LM != null)
        {
            levelNumber = LM.levelNumber;
        }
        
        if (LM != null && levelNumber <= 20)
        {
            repeatInterval = (baseRepeat - 0.05f * levelNumber);
        }
        else
        {
            repeatInterval = (baseRepeat - 0.05f * 20);
        }

        if (levelNumber < 25)
        {
            prefabToSpawn.GetComponent<Movement>().speed = prefabToSpawn.GetComponent<Movement>().baseSpeed + levelNumber * 0.005f;
        }
        else
        {
            prefabToSpawn.GetComponent<Movement>().speed = prefabToSpawn.GetComponent<Movement>().baseSpeed + 0.05f;
        }

        if (hasBeenEnabledBefore || this.transform.position.x < 51)
        {
            StartCoroutine(SpawnTimer());
        }
        hasBeenEnabledBefore = true;
        


    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    // Used to spawn a new game object
    public void SpawnObject()
    {
        
        //Debug.Log(prefabToSpawn.GetComponent<Movement>().speed.ToString() + " is the speed of this " + prefabToSpawn.gameObject.name);
        // Instantiate the prefab at the location of the current SpawnPoint object
        // Quaternion is a data structure used to represent rotations; identity = no rotation
        Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
    }

    //  Used as a timer for spawning animals
    public IEnumerator SpawnTimer()
    {
        while(true)
        {
            SpawnObject();
            yield return new WaitForSeconds(repeatInterval);
        }
    }



}
