﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    // public bool rotateX, rotateY, and rotateZ will determine if this object will rotate on each axis respectively.
    public bool rotateX;
    public bool rotateY;
    public bool rotateZ;

    // public float speedX, speedY, and speedZ will determine the speed this object will rotate on each axis respectively.
    public float speedX;
    public float speedY;
    public float speedZ;

    // Update is called once per frame
    void Update()
    {
        if (rotateX)
        {
            this.transform.Rotate(Vector3.right.normalized * speedX);
        }

        if (rotateY)
        {
            this.transform.Rotate(Vector3.up.normalized * speedY);
        }

        if (rotateZ)
        {
            this.transform.Rotate(Vector3.forward.normalized * speedZ);
        }
    }
}
