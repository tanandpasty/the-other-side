﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioSource audioSource;
    public GameManager gm;
    private void Awake()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameObject[] audio = GameObject.FindGameObjectsWithTag("Music");
        if(audio.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        audioSource = this.GetComponent<AudioSource>();
    }

    private void Update()
    {
        audioSource.mute = gm.musicMuted;
    }

}
