﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindNewLocation : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("CanBePickedUp"))
        {
            Destroy(this.gameObject);
        }
    }
}
