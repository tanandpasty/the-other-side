﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFromSky : MonoBehaviour
{
    //used to represent the current position of the spawner
    Vector3 position;

    //itemArray is used to store references to all of the food prefabs
    [SerializeField]
    private GameObject[] itemArray;

    //OldSpawnItems
    //will take a levelNumber as a parameter and will spawn items for that level
    //public void SpawnItems(int levelNum, int numOfItems)
    //{
        //for(int i = 0; i < numOfItems; i++)
        //{
        //    Instantiate(itemArray[Random.Range(0, 3)], new Vector3(Random.Range(-42 + (levelNum - 1) * 100, -52 + (levelNum * 100)), 3, Random.Range(-13.0f, 33.0f)), Quaternion.identity);
      //  }
    //}
    
    public void SpawnItems(int levelNum, int numOfItems)
    {
        for(int i = 0; i < numOfItems; i++)
        {
            RaycastHit[] checkStrip = Physics.RaycastAll(new Vector3(Random.Range(-42 + (levelNum - 1) * 100, -52 + (levelNum * 100)), 2, 10), Vector3.down, 10.0f);
            if(checkStrip != null)
            {
                int randomNumber = Random.Range(1, 4);
                switch (checkStrip[0].collider.gameObject.name)
                {
                    case "WaterTopStrip(Clone)":
                        if(randomNumber < 2)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, -6), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        }
                        break;
                    case "WaterBottomStrip(Clone)":
                        if (randomNumber < 2)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 26), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        }
                        break;
                    case "grass strip(Clone)":
                        if (randomNumber == 2)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, -6), Quaternion.identity);
                        }
                        else if(randomNumber == 1)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 26), Quaternion.identity);
                        }
                        break;
                    case "levelStart(Clone)":
                        if (randomNumber == 2)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, -6), Quaternion.identity);
                        }
                        else if (randomNumber == 1)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 26), Quaternion.identity);
                        }
                        break;
                    case "waterMiddleStrip(Clone)":
                        if (randomNumber < 2)
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 26), Quaternion.identity);
                        }
                        else
                        {
                            Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, -6), Quaternion.identity);
                        }
                        break;
                    case "grassMiddleStrip(Clone)":
                        Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        break;
                    default:
                        Instantiate(itemArray[Random.Range(0, 4)], new Vector3(checkStrip[0].transform.position.x, 1, 10), Quaternion.identity);
                        break;
                }
            }
        }
    }

}
