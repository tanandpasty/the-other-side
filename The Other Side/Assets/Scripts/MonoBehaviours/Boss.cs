﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public Transform player;
    public float speed = 10;
    public int level;
    // Start is called before the first frame update
    public void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        level = GameObject.FindGameObjectWithTag("LevelGenerator").GetComponent<LevelGenerator>().levelNumber;
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform != null && player != null)
        {
            if(this.transform.localPosition.x >= -38 && this.transform.localPosition.x <= 50 && player.position.x >= (-39 + (100 * level + 1)))
            {
                transform.LookAt(player);
                this.transform.position = Vector3.MoveTowards(this.transform.position, player.position, 1.0f * Time.deltaTime * speed);
            }
        }
    }

    private void OnDisable()
    {

        this.transform.localPosition = ChooseLocation();
    }

    private Vector3 ChooseLocation()
    {
        int randomNumber = Random.Range(1, 101);
        if(randomNumber >= 1 && randomNumber < 33)
        {
            return new Vector3(0.9f, 0f, 9.6f);
        }
        else if(randomNumber >= 33 && randomNumber < 66)
        {
            return new Vector3(0.9f, 0f, -6.8f);
        }
        else if (randomNumber >= 66 && randomNumber < 75)
        {
            return new Vector3(0.9f, 0f, -6.8f);
        }
        else if (randomNumber >= 75 && randomNumber < 85)
        {
            return new Vector3(41.2f, 0f, 10.9f);
        }
        else if (randomNumber >= 85 && randomNumber < 99)
        {
            return new Vector3(9.8f, 0f, 21.8f);
        }
        else
        {
            return new Vector3(-22.2f, 0f, 9f);
        }

    }

}
