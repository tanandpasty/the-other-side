﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool musicMuted;
    public float highScore = 0;
    public int[] unlockedSkins =
    {
        1,
        0,
        0,
        0
    };

    public int currentSkin = 0;

    private void Start()
    {
        musicMuted = false;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.transform.GetChild(0).gameObject.SetActive(false);
        player.transform.GetChild(currentSkin).gameObject.SetActive(true);
        if (scene.name == "Main Menu")
        {
            if (highScore > 10)
            {
                unlockedSkins[1] = 1;
            }
            if (highScore > 25)
            {
                unlockedSkins[2] = 1;
            }
            if (highScore > 50)
            {
                unlockedSkins[3] = 1;
            }
        }
    }

    private void Awake()
    {
        if(GameObject.FindGameObjectsWithTag("GameManager").Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void MusicOn()
    {
        musicMuted = false;
    }
    public void MusicOff()
    {
        musicMuted = true;
    }

}
